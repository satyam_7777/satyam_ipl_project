let iplDataSet=require('./matches.json')
let deliveries=require('./deliveries.json')
//function 1

 const iplMatchesPerYear=(iplDataSet)=>{
 const totalMatchPerYear=iplDataSet.reduce((totalMatchPerYear, match) => {
    const currentYear=match["season"]
    if(totalMatchPerYear[currentYear]){
        totalMatchPerYear[currentYear]+= 1
    }
    else
    {
        totalMatchPerYear[currentYear]= 1
    }
    return totalMatchPerYear
    },{})
    return totalMatchPerYear
 }
   //console.log(iplMatchesPerYear(iplDataSet))
    

    /////////////////////////////////////////////////////////////////////////////////
    //function 2


const matchesWonPerTeamPerYear=(iplDataSet)=>{
        
  const matchesWonPerTeamPerYear = iplDataSet.reduce((matchesWonPerTeam, currentYear) => {
    if (matchesWonPerTeam.hasOwnProperty(currentYear.season)) {
      if (matchesWonPerTeam[currentYear.season].hasOwnProperty(currentYear.winner)) 
         {
            matchesWonPerTeam[currentYear.season][currentYear.winner] += 1;
          } 
      else{
              matchesWonPerTeam[currentYear.season][currentYear.winner] = 1;
             }
         } 
      else{
             matchesWonPerTeam[currentYear.season] = {};
            matchesWonPerTeam[currentYear.season][currentYear.winner] = 1;
            }

          return matchesWonPerTeam;
        }, {})
        
  return matchesWonPerTeamPerYear
    
    }
    


//console.log(matchesWonPerTeamPerYear(iplDataSet))


/////////////////////////////////////////////////////////////////////////////////////////
// function 3


const extraRunByEachTeam2016=(iplDataSet,deliveries)=>{

    const matchId2016=iplDataSet.filter(match=>match.season=='2016').map(match=>match.id)
    //console.log(matchId2016)
    
    const deliverydata2016=deliveries.filter(eachDelivery=> 
    matchId2016.indexOf(eachDelivery.match_id)!==-1)
    //console.log(deliverydata2016)
    
    const runsConcededByEachTeam=deliverydata2016.reduce((runsConcededByEachTeam, currdelivery) => {
    const battingTeam=currdelivery["batting_team"]
    if(runsConcededByEachTeam[battingTeam]){
    runsConcededByEachTeam[battingTeam]+= parseInt(currdelivery['extra_runs'],10)
    }
    else
    {
    runsConcededByEachTeam[battingTeam]= parseInt(currdelivery['extra_runs'],10)
    }
    return runsConcededByEachTeam
    },{})
    return runsConcededByEachTeam
 }
 //console.log(extraRunByEachTeam2016(iplDataSet,deliveries))
 

 //////////////////////////////////////////////////////////////////////////////////
 //function 4
 
const topTenEconomicalBowler2015=(iplDataSet,deliveries)=>{

    const matchId2015=iplDataSet.filter(match=>match.season=='2015').map(match=>match.id)
    
    const deliverydata2015=deliveries.filter(eachDelivery=> 
    matchId2015.indexOf(eachDelivery.match_id)!==-1)
    
    const runsConcededByEachBowler=deliverydata2015.reduce(
    (runsConcededByEachBowler, currdelivery) => {
    const bowler=currdelivery["bowler"]
    if(runsConcededByEachBowler[bowler]){
    runsConcededByEachBowler[bowler]+= parseInt(currdelivery['total_runs'],10)
    }
    else
    {
    runsConcededByEachBowler[bowler]= parseInt(currdelivery['total_runs'],10)
    }
    return runsConcededByEachBowler
    },{}
    )
    
    const totalBowlsPerBowler=deliverydata2015.reduce((totalBowlsPerBowler, currdelivery) => {
    const bowler=currdelivery["bowler"]
    if(totalBowlsPerBowler[bowler]){
    totalBowlsPerBowler[bowler]+= 1
    }
    else
    {
    totalBowlsPerBowler[bowler]= 1
    }
    return totalBowlsPerBowler
    },{})
    
    let economyrateEachBowler=totalBowlsPerBowler
    for(let i in totalBowlsPerBowler)
    {
    economyrateEachBowler[i]=runsConcededByEachBowler[i]*6/totalBowlsPerBowler[i];
    }
    
    let economy=economyrateEachBowler;
    let economyArray = [];
    for (let i in economy) {
    economyArray.push([i, economy[i]]);
    }
    
    economyArray.sort(function(a, b) {
    return a[1] - b[1];
    });

    let topTenEconomicalBowler=[]
    for(let i=0;i<10;i++)
    {
      topTenEconomicalBowler.push(economyArray[i])
    }

    return topTenEconomicalBowler
 }
    //console.log(topTenEconomicalBowler2015(iplDataSet,deliveries))
    
    module.exports={ iplMatchesPerYear:iplMatchesPerYear,
      matchesWonPerTeamPerYear:matchesWonPerTeamPerYear,
        extraRunByEachTeam2016:
        extraRunByEachTeam2016,
        topTenEconomicalBowler2015:
        topTenEconomicalBowler2015

    }
    
    
