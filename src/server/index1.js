// result transfer in json file in output directory

const path=require('path')
const  fileURLToPath =require('url')
const fs=require('fs')
const ipl=require('./ipl.js')

const matchesFilePath = "../data/matches.json";
const deliveriesFilePath = "../data/deliveries.json";


const deliveries= JSON.parse(fs.readFileSync(path.resolve(__dirname, deliveriesFilePath)));
const iplData = JSON.parse(fs.readFileSync(path.resolve(__dirname, matchesFilePath)));

let matchesPerYear = ipl.iplMatchesPerYear(iplData);
let matchesWonPerTeamPerYear = ipl.matchesWonPerTeamPerYear(iplData);
let extraRunByEachTeam2016 = ipl.extraRunByEachTeam2016(iplData, deliveries);
let topTenEconomicalBowler2015 = ipl.topTenEconomicalBowler2015(iplData, deliveries);

function exportDataToJson(fileName, data) {
    const outputPath = "../public/output";
    fs.writeFileSync(path.resolve(__dirname, outputPath, `${fileName}.json`), JSON.stringify(data), "utf-8", (err) => {
        if (err) { console.log(err); }
    });
}

exportDataToJson("matchesPerYear", matchesPerYear);
exportDataToJson("matchesWonPerTeamPerYear", matchesWonPerTeamPerYear);
exportDataToJson("extraRunByEachTeam2016", extraRunByEachTeam2016);
exportDataToJson("topTenEconomicalBowler2015", topTenEconomicalBowler2015);
